const   gulp        = require('gulp'),

        sass        = require('gulp-sass'),

        sourcemaps  = require('gulp-sourcemaps'),

        livereload  = require('gulp-livereload');




/**

 * The default task will run development tasks because the onload nodejs docker container will easily run this.

 */

gulp.task('default', ['dev'], function() {});


/**

 * This task will run production tasks for the styles.

 */

gulp.task('sass:prod', function() {

    gulp.src('./sass/style.scss')

        .pipe(sass())

        .pipe(gulp.dest('./dist/css'))

});


/**

 * This will do dev tasks for the scss.

 */

gulp.task('sass:dev', function() {

    gulp.src('./sass/style.scss')

        .pipe(sourcemaps.init())

        .pipe(sass())

        .pipe(sourcemaps.write())

        .pipe(gulp.dest('./dist/css'))

        .pipe(livereload());

});


/**

 * This task will run development tasks which include using a live reload server.

 */

gulp.task('dev',['sass:dev'], function() {

    livereload.listen();

    gulp.watch('./sass/**/*.scss', ['sass:dev']);

});
